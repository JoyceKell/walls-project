import Header from "../../components/Header";
import Calculate from "../../components/Calculate";
import Start from "../../components/Start";

export default function Main() {
  return (
    <>
      <Header />
      <Start />
      <Calculate />
    </>
  );
}
