import painter from "../../assets/painter.jpg";

export default function Start() {
  return (
    <div
      id="start"
      className="py-28 flex md:flex-row  md:justify-center md:space-x-11 bg-slate-300 before:content-[''] before:block before:h-80 before:-mt-80 flex-col items-center"
    >
      <div className="pb-11 ">
        <img
          src={painter}
          className="rounded-lg object-scale-down max-w-sm h-64 md:h-80 transition-shadow ease-in-out duration-300 shadow-none hover:shadow-xl"
        />
      </div>
      <div className="p-11">
        <span className="text-2xl text-center mt-20">
          Descubra a quantidade necessária para pintar uma sala!
        </span>
        <p className="mt-3">
          Informe as medidas de cada parede e quantas janelas possuem cada uma.
        </p>
        <p className="mb-5">
          Mostraremos quantas latas e quantas mls de tinta serão necessários
          para à sua pintura!
        </p>
        <a
          href="#calculate"
          className=" bg-transparent hover:bg-gray-800 text-gray-700 font-semibold hover:text-white py-2 px-4 border border-gray-800 hover:border-transparent rounded"
        >
          Calcular
        </a>
      </div>
    </div>
  );
}
