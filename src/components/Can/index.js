import { useEffect } from "react";

export default function Can({ can05, can18, can36, can25 }) {
  return (
    <>
      <div className="flex justify-center">
        <div className="rounded-lg shadow-lg bg-white max-w-sm p-10">
          <p>Para pintar uma sala com essas medidas, será necessário:</p>
          <p>{can18} latas de 18L</p>
          <p>{can36} latas de 3.6L</p>
          <p>{can25} latas de 2.5L</p>
          <p>{can05} latas de 0.5L</p>
        </div>
      </div>
    </>
  );
}
