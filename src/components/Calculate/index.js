import { useState, useEffect } from "react";
import * as yup from "yup";
import Can from "../Can";

export default function Calcular() {
  const [heightWalls, setHeightWalls] = useState(100);
  const [widthWalls, setWidthWalls] = useState(100);
  const [p1Door, setP1Door] = useState(0);
  const [p1Window, setP1Window] = useState(0);

  const [p2Door, setP2Door] = useState(0);
  const [p2Window, setP2Window] = useState(0);

  const [p3Door, setP3Door] = useState(0);
  const [p3Window, setP3Window] = useState(0);

  const [p4Door, setP4Door] = useState(0);
  const [p4Window, setP4Window] = useState(0);

  const [showErrors, setShowErrors] = useState(false);
  const [messageErrors, setMessageErrors] = useState([]);
  const [showCalc, setShowCalc] = useState(false);

  const [can05, setCan05] = useState(0);
  const [can25, setCan25] = useState(0);
  const [can36, setCan36] = useState(0);
  const [can18, setCan18] = useState(0);

  const userSchema = yup.object().shape({
    heightWalls: yup
      .number()
      .min(
        100,
        "insira um número maior ou igual a 100 no campo altura da parede 1"
      )
      .max(
        500,
        "insira um número menor ou igual a 500 no campo largura da parede 1"
      )
      .required()
      .positive("O número deve ser positivo")
      .integer(),
    widthWalls: yup
      .number()
      .min(
        100,
        "insira um número maior ou igual a 100 no campo largura da parede 1"
      )
      .max(
        500,
        "insira um número menor ou igual a 500 no campo largura da parede 1"
      )
      .required()
      .positive("O número deve ser positivo")
      .integer(),
    p1Door: yup.number().required().integer(),
    p1Window: yup.number().required().integer(),

    p2Door: yup.number().required().integer(),
    p2Window: yup.number().required().integer(),

    p3Door: yup.number().required().integer(),
    p3Window: yup.number().required().integer(),

    p4Door: yup.number().required().integer(),
    p4Window: yup.number().required().integer(),
  });

  const paint = 2500;
  let i = 0;

  const window = 24000;
  const door = 15200;

  var paint05 = 0;
  var paint25 = 0;
  var paint36 = 0;
  var paint18 = 0;

  function verifyDoor(heightWall, numDoor) {
    if (numDoor > 0) {
      if (heightWall - 80 < 30) {
        setShowCalc(false);
        alert(
          "A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta"
        );
      }
    }
  }

  function verifyArea(doorAndWindowArea, totalArea) {
    if (doorAndWindowArea > totalArea / 2) {
      setShowCalc(false);
      alert(
        "O total de área das portas e janelas deve ser no máximo 50% da área de parede"
      );
    }
  }

  function calcular() {
    let paints = 0;

    let sumOfWall = heightWalls * widthWalls * 4;

    sumOfWall =
      sumOfWall -
      window * p1Door -
      door * p1Door -
      window * p2Door -
      door * p2Door -
      window * p3Door -
      door * p3Door -
      window * p4Door -
      door * p4Door;

    while (sumOfWall >= 2500) {
      paints = paints + 1;
      sumOfWall = sumOfWall - 2500;
    }

    while (paints > 0) {
      if (paints >= 18) {
        paint18 = paint18 + 1;
        paints = paints - 18;
      } else if (paints >= 3.6) {
        paint36 = paint36 + 1;
        paints = paints - 3.6;
      } else if (paints >= 2.5) {
        paint25 = paint25 + 1;
        paints = paints - 2.5;
      } else if (paints >= 0.5) {
        paint05 = paint05 + 1;
        paints = paints - 0.5;
      } else {
        paint05 = paint05 + 1;
        paints = 0;
      }
    }

    setCan05(paint05);
    setCan18(paint18);
    setCan36(paint36);
    setCan25(paint25);
  }

  function handleSubmit(e) {
    e.preventDefault();
    setShowCalc(true);
    const submitForm = async () => {
      const userFormData = {
        heightWalls: Number(heightWalls),
        widthWalls: Number(widthWalls),
        p1Door: Number(p1Door),
        p1Window: Number(p1Window),

        p2Door: Number(p2Door),
        p2Window: Number(p2Window),

        p3Door: Number(p3Door),
        p3Window: Number(p3Window),

        p4Door: Number(p4Door),
        p4Window: Number(p4Window),
      };
      const isValid = await userSchema.isValid(userFormData);
      if (isValid) {
        verifyDoor(heightWalls, p1Door);

        verifyArea(p1Door * door + p1Window * window, heightWalls * widthWalls);

        calcular();
        setMessageErrors([]);
      } else {
        userSchema
          .validate(userFormData, {
            abortEarly: false,
          })
          .catch(function (e) {
            e.inner.forEach((err) => {
              if (messageErrors.includes(e.message) === false) {
                setMessageErrors([err.message]);
                setShowErrors(true);
              }
            });
          });
      }
    };
    submitForm();
  }

  return (
    <div
      id="calculate"
      className="py-28 flex flex-col md:flex-row items-center justify-center md:space-x-11"
    >
      <form className="w-full max-w-sm">
        <div>
          <div className="ml-1">
            <label className=" text-gray-700 text-sm font-bold mb-2">
              Altura e largura das paredes
            </label>
          </div>
          <div className="grid grid-cols-4 gap-4 mb-5">
            <input
              className="bg-gray-200 w-24 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
              type="number"
              placeholder="altura"
              value={heightWalls}
              onChange={(e) => setHeightWalls(e.target.value)}
              required
            />
            <input
              className="bg-gray-200 w-24 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
              type="number"
              placeholder="largura"
              value={widthWalls}
              required
              onChange={(e) => setWidthWalls(e.target.value)}
            />
          </div>
        </div>
        <div className="ml-1">
          <label className=" text-gray-700 text-sm font-bold mb-2">
            portas e janelas da parede 01
          </label>
        </div>
        <div className="grid grid-cols-4 gap-4 mb-5">
          <input
            className="bg-gray-200 w-24 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
            type="number"
            placeholder="portas"
            value={p1Door}
            required
            onChange={(e) => setP1Door(e.target.value)}
          />
          <input
            className="bg-gray-200 w-24 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
            type="number"
            placeholder="janelas"
            value={p1Window}
            required
            onChange={(e) => setP1Window(e.target.value)}
          />
        </div>
        <div className="ml-1">
          <label className=" text-gray-700 text-sm font-bold mb-2">
            portas e janelas da parede 02
          </label>
        </div>
        <div className="grid grid-cols-4 gap-4 mb-5">
          <input
            className="bg-gray-200 w-24 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
            type="number"
            placeholder="portas"
            value={p2Door}
            required
            onChange={(e) => setP2Door(e.target.value)}
          />
          <input
            className="bg-gray-200 w-24 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
            type="number"
            placeholder="janelas"
            value={p2Window}
            required
            onChange={(e) => setP2Window(e.target.value)}
          />
        </div>
        <div className="ml-1">
          <label className=" text-gray-700 text-sm font-bold mb-2">
            portas e janelas da parede 03
          </label>
        </div>
        <div className="grid grid-cols-4 gap-4 mb-5">
          <input
            className="bg-gray-200 w-24 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
            type="number"
            placeholder="portas"
            value={p3Door}
            required
            onChange={(e) => setP3Door(e.target.value)}
          />
          <input
            className="bg-gray-200 w-24 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
            type="number"
            placeholder="janelas"
            value={p3Window}
            required
            onChange={(e) => setP3Window(e.target.value)}
          />
        </div>
        <div className="ml-1">
          <label className=" text-gray-700 text-sm font-bold mb-2">
            portas e janelas da parede 04
          </label>
        </div>
        <div className="grid grid-cols-4 gap-4 mb-7">
          <input
            className="bg-gray-200 w-24 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
            type="number"
            placeholder="portas"
            value={p4Door}
            required
            onChange={(e) => setP4Door(e.target.value)}
          />
          <input
            className="bg-gray-200 w-24 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
            type="number"
            placeholder="janelas"
            value={p4Window}
            required
            onChange={(e) => setP4Window(e.target.value)}
          />
        </div>
        <button
          type="submit"
          onClick={handleSubmit}
          className="bg-gray-600 hover:bg-gray-900 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Calcular
        </button>
        <div className="p-5">
          <ul>
            {showErrors &&
              messageErrors.map((err) => (
                <li className="text-red-500" key={(i = i + 1)}>
                  {err}
                </li>
              ))}
          </ul>
        </div>
      </form>
      <div>
        {showCalc && (
          <Can can05={can05} can18={can18} can25={can25} can36={can36} />
        )}
      </div>
    </div>
  );
}
